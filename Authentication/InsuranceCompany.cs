﻿using nOutageAPI.Entities;

namespace nOutageAPI.Authentication
{
    public class InsuranceCompany
    {
        public Guid Id { get; set; }
        public string Title { get; set; }

        public List<ApplicationUser> Users { get; set; }
        public List<Customer> Customers { get; set; }
    }
}
