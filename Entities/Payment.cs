﻿namespace nOutageAPI.Entities
{
    public class Payment
    {
        public Guid Id { get; set; }
        public long Amount { get; set; }
        public Outage Outage { get; set; }
        public Service Service { get; set; }
        public bool IsPaid { get; set; } = false;
    }
}
