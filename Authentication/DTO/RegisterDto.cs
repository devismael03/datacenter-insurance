﻿using System.ComponentModel.DataAnnotations;

namespace nOutageAPI.Authentication.DTO
{
    public class RegisterDto
    { 
        [EmailAddress]
        [Required]
        public string Email { get; set; }

        [Required]
        public string Password { get; set; }

        [Required]
        public string InsuranceCompanyId { get; set; }
    }
}
