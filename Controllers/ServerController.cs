﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nOutageAPI.Entities;

namespace nOutageAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class ServerController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly ZabbixClient _zabbixClient;

        public ServerController(ApplicationDbContext context, ZabbixClient zabbixClient)
        {
            _context = context;
            _zabbixClient = zabbixClient;
        }

        [HttpGet]
        public async Task<IActionResult> GetServers()
        {
            var servers = await _context.Servers.Select(server => new {Id = server.Id, Title = server.Title }).ToListAsync();

            return Ok(servers);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetServer(Guid id)
        {
            var server = await _context.Servers.Where(server => server.Id == id)
                                    .Select(server => new {
                                        Title = server.Title,
                                        IsUp = server.IsUp,
                                        DataCenter = server.DataCenter.Title,
                                        Outages = server.Outages.Select(outage => new
                                        {
                                            Id = outage.Id,
                                            StartDate = outage.StartDate.ToString(),
                                            EndDate = outage.EndDate.ToString(),
                                            IsResolved = outage.Resolved
                                        }).ToList()
                                    }).FirstOrDefaultAsync();

            if (server == null)
            {
                return BadRequest();
            }

            return Ok(server);
        }

        [HttpGet("{id:guid}/stats")]
        public async Task<IActionResult> GetStats(Guid id, string? type)
        {
            if(type == null)
            {
                return BadRequest();
            }

            var server = await _context.Servers.Where(server => server.Id == id).FirstOrDefaultAsync();

            if (server == null)
            {
                return BadRequest();
            }

            if (type == "ram")
            {
                var result = await _zabbixClient.GetValue(server.Hostname, "vm.memory.size[available]");

                return Ok(result);
            }
            else if(type == "cpu")
            {
                var result = await _zabbixClient.GetValue(server.Hostname, "system.cpu.util[,user]");

                return Ok(result);
            }

            return Ok();
        }


        [HttpPost("alert")]
        [AllowAnonymous]
        public async Task<IActionResult> AlertServer([FromBody] AlertEvent alert)
        {
            var server = await _context.Servers.Where(server => server.Hostname == alert.Host).Include(server => server.Services).ThenInclude(service => service.Servers)
                                                                                                .Include(server => server.Services).ThenInclude(service => service.Customer)
                                                                                                .FirstOrDefaultAsync();
                                                                                    
            if(server == null)
            {
                return BadRequest();
            }

            if (alert.ProblemResolvedAt != null)
            {
                // Handle the 'problem resolved' event

                server.IsUp = true;

                var outage = await _context.Outages.Where(outage => outage.Resolved == false && outage.Server.Id == server.Id).FirstOrDefaultAsync();

                if(outage == null)
                {
                    return BadRequest();
                }

                var priorityCoefficients = new Dictionary<Priority, double> {
                    { Priority.Low,  0.4},
                    { Priority.Medium,  0.6},
                    { Priority.High,  0.8},
                    { Priority.Critical, 1}
                };

                outage.EndDate = DateTime.UtcNow;
                outage.Resolved = true;

                TimeSpan difference = outage.EndDate - outage.StartDate;
                long downTimeMinutes = ((int)difference.TotalMinutes);


                foreach (var service in server.Services)
                {
                    service.CurrentDowntimeMinutes += downTimeMinutes;

                    long totalPayment = 0;
                    var basePayment = service.Customer.BaseHourlyCoverageAmount;
                    long downtimeMinutestExceedingSla = 0;
                    int serviceResidingServerCount = service.Servers.Count();

                    if(service.CurrentDowntimeMinutes + downTimeMinutes <= service.SLAMinutes)
                    {
                        continue;
                    }
                    else if(service.CurrentDowntimeMinutes <= service.SLAMinutes)
                    {
                        downtimeMinutestExceedingSla = service.CurrentDowntimeMinutes + downTimeMinutes - service.SLAMinutes;
                    }
                    else
                    {
                        downtimeMinutestExceedingSla = downTimeMinutes;
                    }


                    totalPayment += (downtimeMinutestExceedingSla * basePayment) /60;
                    totalPayment = (long) (totalPayment * priorityCoefficients[service.Priority]);
                    totalPayment = totalPayment / serviceResidingServerCount;

                    var newPayment = new Payment { Id = Guid.NewGuid(), Amount = totalPayment, IsPaid = false, Outage = outage, Service = service };
                    await _context.Payments.AddAsync(newPayment);

                //customer.basehourlycoverageamount, service.sla, service.priority, service.currentdowntime, 1/serviceolanserversayi
                //payment yarat, paymentlara elave et, currentdowntime minute artir
                }
            }
            else if (alert.ProblemStartedAt != null)
            {
                // Handle the 'problem started' event
                server.IsUp = false;
                var outage = new Outage { Id = Guid.NewGuid(), Server = server, StartDate = DateTime.UtcNow, EndDate = DateTime.UtcNow, Resolved = false };
                await _context.Outages.AddAsync(outage);
            }
            else
            {
                return BadRequest();
            }

            await _context.SaveChangesAsync();
            return Ok();
        }
    }
    public class AlertEvent
    {
        public string? ProblemResolvedAt { get; set; }
        public string? ProblemStartedAt { get; set; }
        public string ProblemName { get; set; }
        public string? ProblemDuration { get; set; }
        public string Host { get; set; }
        public string Severity { get; set; }
        public string? OperationalData { get; set; }
        public string OriginalProblemId { get; set; }
        public string TriggerUrl { get; set; }
    }

}
