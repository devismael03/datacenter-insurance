﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nOutageAPI.Entities;
using System.ComponentModel.DataAnnotations;

namespace nOutageAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class CustomerController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public CustomerController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetCustomers()
        {
            var companyId = this.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == "CompanyId")?.Value;
            if (companyId == null)
            {
                return Unauthorized();
            }

            var customers = await _context.Customers.Where(customer => customer.InsurerId == Guid.Parse(companyId)).Select(customer => new { 
                Id = customer.Id,
                Title = customer.Title,
                ServiceCount = customer.Services.Count(),
                BaseHourlyCoverageAmount = customer.BaseHourlyCoverageAmount

            }).ToListAsync();

            return Ok(customers);
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetCustomer(Guid id)
        {
            var companyId = this.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == "CompanyId")?.Value;
            if (companyId == null)
            {
                return Unauthorized();
            }

            var customer = await _context.Customers.Where(customer => customer.InsurerId == Guid.Parse(companyId) && customer.Id == id).Select(customer => new {
                Id = customer.Id,
                Title = customer.Title,
                Services = customer.Services.Select(service => new
                {
                    Id = service.Id,
                    Title = service.Title,
                    ServerCount = service.Servers.Count(),
                    UpServerCount = service.Servers.Where(server => server.IsUp).Count()
                })

            }).FirstOrDefaultAsync();

            if (customer == null)
            {
                return BadRequest();
            }

            return Ok(customer);
        }


        [HttpPost]
        public async Task<IActionResult> CreateCustomer(CreateCustomerDto model)
        {
            var companyId = this.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == "CompanyId")?.Value;
            if (companyId == null)
            {
                return Unauthorized();
            }
            var newCustomer = new Customer { Id = Guid.NewGuid(), BaseHourlyCoverageAmount = model.BaseHourlyCoverageAmount ?? 0, Title = model.Title, InsurerId = Guid.Parse(companyId)};
            await _context.Customers.AddAsync(newCustomer);

            await _context.SaveChangesAsync();

            return Ok();
        }
    }

    public class CreateCustomerDto
    {
        [Required]
        public string Title { get; set; }

        [Required]
        public int? BaseHourlyCoverageAmount { get; set; }

    }
}
