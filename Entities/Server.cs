﻿namespace nOutageAPI.Entities
{
    public class Server
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public string Hostname { get; set; }
        public bool IsUp { get; set; }
        public DataCenter DataCenter { get; set; }
        public List<Service> Services { get; set; }
        public List<Outage> Outages { get; set; }
    }
}
