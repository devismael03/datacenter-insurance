﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;

namespace nOutageAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class OutageController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public OutageController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetLatestOutages()
        {
            var outages = await _context.Outages.OrderByDescending(outage => outage.StartDate).Select(outage => new {Id = outage.Id, Server = outage.Server.Title, StartDate = outage.StartDate.ToString(), IsResolved = outage.Resolved, DataCenter = outage.Server.DataCenter.Title }).Take(3).ToListAsync();
            return Ok(outages);
        }


        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetOutage(Guid id)
        {

            var outage = await _context.Outages.Where(outage => outage.Id == id)
                                .Select(outage => new {Id = outage.Id, IsResolved = outage.Resolved, Server = outage.Server.Title,
                                    StartDate = outage.StartDate.ToString(),
                                    EndDate = outage.EndDate.ToString(),
                                    AffectedServices = outage.Server.Services.Select(service => new { Id = service.Id, CustomerTitle = service.Customer.Title, Title = service.Title, Priority = service.Priority }),
                                })
                .FirstOrDefaultAsync();

            if (outage == null)
            {
                return BadRequest();
            }

            return Ok(outage);
        }
    }
}
