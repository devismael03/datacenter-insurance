﻿namespace nOutageAPI.Authentication.DTO
{
    public class AuthenticationResponse
    {
        public string AccessToken { get; set; }
    }
}
