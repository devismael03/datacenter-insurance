﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using nOutageAPI.Entities;
using System.ComponentModel.DataAnnotations;

namespace nOutageAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class ServiceController : ControllerBase
    {
        private readonly ApplicationDbContext _context;

        public ServiceController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet("{id:guid}")]
        public async Task<IActionResult> GetService(Guid id)
        {
            var companyId = this.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == "CompanyId")?.Value;
            if (companyId == null)
            {
                return Unauthorized();
            }

            var service = await _context.Services.Where(service => service.Id == id && service.Customer.InsurerId == Guid.Parse(companyId))
                                    .Select(service => new {Id = service.Id, CustomerTitle = service.Customer.Title, Title = service.Title,
                                        ServerCount = service.Servers.Count(),
                                        UpServerCount = service.Servers.Where(server => server.IsUp).Count(),
                                        SLAMinutes = service.SLAMinutes,
                                        UsedSLAMinutes = service.CurrentDowntimeMinutes,
                                        Servers = service.Servers.Select(server => new { Id = server.Id, Title = server.Title, Datacenter = server.DataCenter.Title, IsUp = server.IsUp }),
                                        Payments = service.Payments.Select(payment => new { Id = payment.Id, Amount = payment.Amount, IsPaid = payment.IsPaid, OutageId = payment.Outage.Id })
                                    }).FirstOrDefaultAsync();

            if (service == null)
            {
                return BadRequest();
            }

            return Ok(service);
        }


        [HttpPost]
        public async Task<IActionResult> CreateService(CreateServiceDto model)
        {
            if(model.Servers == null)
            {
                return BadRequest();
            }

            var companyId = this.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == "CompanyId")?.Value;
            if (companyId == null)
            {
                return Unauthorized();
            }

            var customer = await _context.Customers.Where(customer => customer.InsurerId == Guid.Parse(companyId) && customer.Id == model.CustomerId).FirstOrDefaultAsync();

            if (customer == null)
            {
                return Unauthorized();
            }

            var newService = new Service { Id = Guid.NewGuid(), Title = model.Title, SLAMinutes = model.SLAMinutes ?? 0, CurrentDowntimeMinutes = 0, Priority = model.Priority ?? Priority.Low, Customer = customer, Servers = new List<Server>()};

            foreach(var serverId in model.Servers)
            {
                var server = await _context.Servers.Where(server => server.Id == Guid.Parse(serverId)).FirstOrDefaultAsync();
                if(server != null)
                {
                    newService.Servers.Add(server);
                }
            }

            await _context.Services.AddAsync(newService);
            await _context.SaveChangesAsync();

            return Ok();
        }
    }

    public class CreateServiceDto
    {
        [Required]
        public Guid? CustomerId { get; set; }

        [Required]
        public string Title { get; set; }

        [Required]
        public int? SLAMinutes { get; set; }

        [Required]
        public Priority? Priority { get; set; }

        [Required]
        public List<string>? Servers { get; set; }

    }
}
