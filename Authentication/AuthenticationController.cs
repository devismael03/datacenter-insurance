﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using nOutageAPI.Authentication.DTO;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using System.Text;

namespace nOutageAPI.Authentication
{
    [ApiController]
    [Route("api/[controller]")]
    public class AuthenticationController : ControllerBase
    {
        private readonly ApplicationDbContext _context;
        private readonly UserManager<ApplicationUser> _userManager;
        private readonly IConfiguration _configuration;

        public AuthenticationController(UserManager<ApplicationUser> userManager, IConfiguration configuration, ApplicationDbContext context)
        {
            _userManager = userManager;
            _configuration = configuration;
            _context = context;
        }

        [HttpPost]
        [Route("login")]
        public async Task<ActionResult> Login([FromBody] LoginDto model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var user = await _userManager.FindByEmailAsync(model.Email);
            if (user != null && await _userManager.CheckPasswordAsync(user, model.Password))
            {
                var company = await _context.InsuranceCompanies.FirstOrDefaultAsync(company => company.Id == user.CompanyId);
                
                if(company == null)
                {
                    return BadRequest();
                }

                var authClaims = new List<Claim>
                {
                    new Claim("Id",user.Id),
                    new Claim("CompanyName", company.Title),
                    new Claim("CompanyId", company.Id.ToString()),
                    new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString()),
                };

                var token = GenerateJwt(authClaims);


                return Ok(new AuthenticationResponse
                {
                    AccessToken = new JwtSecurityTokenHandler().WriteToken(token),
                });
            }
            return Unauthorized();
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterDto model)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var userExists = await _userManager.FindByEmailAsync(model.Email);
            if (userExists != null)
                return StatusCode(StatusCodes.Status400BadRequest, new { Message = "User already exists!" });

            var insuranceCompany =  await _context.InsuranceCompanies.FirstOrDefaultAsync(company => company.Id == Guid.Parse(model.InsuranceCompanyId));
            
            if(insuranceCompany == null)
            {
                return StatusCode(StatusCodes.Status400BadRequest, new { Message = "No such company!" });
            }
            ApplicationUser user = new ApplicationUser()
            {
                Email = model.Email,
                UserName = model.Email,
                Company = insuranceCompany,
                SecurityStamp = Guid.NewGuid().ToString(),
            };


            var result = await _userManager.CreateAsync(user, model.Password);
            if (!result.Succeeded)
                return StatusCode(StatusCodes.Status500InternalServerError, new { Message = "User creation failed!" });

            return Ok(new { Message = "User created successfully!" });
        }


        private JwtSecurityToken GenerateJwt(List<Claim> authClaims)
        {
            var authSigningKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes("defaultloginschemajwt"));

            var token = new JwtSecurityToken(
                expires: DateTime.UtcNow.AddHours(6),
                claims: authClaims,
                signingCredentials: new SigningCredentials(authSigningKey, SecurityAlgorithms.HmacSha256)
                );

            return token;
        }
    }
}
