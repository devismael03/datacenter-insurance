﻿using nOutageAPI.Authentication;

namespace nOutageAPI.Entities
{
    public class Customer
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public long BaseHourlyCoverageAmount { get; set; }
        public Guid InsurerId { get; set; }
        public InsuranceCompany Insurer { get; set; }
        public List<Service> Services { get; set; }
    }
}
