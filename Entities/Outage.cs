﻿namespace nOutageAPI.Entities
{
    public class Outage
    {
        public Guid Id { get; set; }
        public Server Server { get; set; }
        public DateTime StartDate { get; set; }
        public DateTime EndDate { get; set; }
        public bool Resolved { get; set; } = false;
        public List<Payment> Payments { get; set; }
    }
}
