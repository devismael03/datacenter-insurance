﻿using System.Text;
using System.Text.Json;

namespace nOutageAPI
{
    public class ZabbixHistoryItem
    {
        public string Clock { get; set; }
        public string Value { get; set; }
    }

    public class ZabbixClient
    {
        private readonly HttpClient _httpClient;

        public ZabbixClient(HttpClient httpClient)
        {
            _httpClient = httpClient;
        }

        private async Task<string> GetHostId(string hostname)
        {
            var payload = new
            {
                jsonrpc = "2.0",
                method = "host.get",
                @params = new
                {
                    output = new[] { "hostid" },
                    filter = new
                    {
                        host = new[] { hostname }
                    }
                },
                id = 1
            };

            var jsonPayload = JsonSerializer.Serialize(payload);
            var content = new StringContent(jsonPayload, Encoding.UTF8, "application/json-rpc");

            var response = await _httpClient.PostAsync("", content); 
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();

            using var jsonDoc = JsonDocument.Parse(responseBody);
            var hostId = jsonDoc.RootElement.GetProperty("result").EnumerateArray().FirstOrDefault().GetProperty("hostid").GetString();

            return hostId;
        }

        private async Task<string> GetItemId(string hostid, string keyname)
        {
            var payload = new
            {
                jsonrpc = "2.0",
                method = "item.get",
                @params = new
                {
                    output = new[] { "itemids", "key_", "name" },
                    hostids=hostid,
                    search = new
                    {
                        key_ = keyname
                    }
                },
                id = 1
            };

            var jsonPayload = JsonSerializer.Serialize(payload);
            var content = new StringContent(jsonPayload, Encoding.UTF8, "application/json-rpc");

            var response = await _httpClient.PostAsync("", content);
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();

            using var jsonDoc = JsonDocument.Parse(responseBody);
            var itemId = jsonDoc.RootElement.GetProperty("result").EnumerateArray().FirstOrDefault().GetProperty("itemid").GetString();

            return itemId;
        }
        public async Task<IEnumerable<ZabbixHistoryItem>> GetValue(string hostname, string keyname)
        {
            var hostId = await GetHostId(hostname);
            var itemId = await GetItemId(hostId, keyname);

            var history = 3;
            if(keyname == "system.cpu.util[,user]")
            {
                history = 0;
            }
            var payload = new
            {
                jsonrpc = "2.0",
                method = "history.get",
                @params = new
                {
                    output = "extend",
                    history = history,
                    itemids = itemId,
                    sortfield = "clock",
                    sortorder = "DESC",
                    limit = 40
                },
                id = 1
            };

            var jsonPayload = JsonSerializer.Serialize(payload);
            var content = new StringContent(jsonPayload, Encoding.UTF8, "application/json-rpc");

            var response = await _httpClient.PostAsync("", content); // Assuming the base URL is set in HttpClient
            response.EnsureSuccessStatusCode();
            var responseBody = await response.Content.ReadAsStringAsync();

            using var jsonDoc = JsonDocument.Parse(responseBody);
            var items = new List<ZabbixHistoryItem>();
            foreach (var resultItem in jsonDoc.RootElement.GetProperty("result").EnumerateArray())
            {
                items.Add(new ZabbixHistoryItem
                {
                    Clock = resultItem.GetProperty("clock").GetString(),
                    Value = resultItem.GetProperty("value").GetString()
                });
            }

            return items;
        }
    }

    
}
