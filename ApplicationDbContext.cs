﻿using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using nOutageAPI.Authentication;
using nOutageAPI.Entities;

namespace nOutageAPI
{
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public DbSet<InsuranceCompany> InsuranceCompanies { get; set; }
        public DbSet<Customer> Customers { get; set; }
        public DbSet<DataCenter> DataCenters { get; set; }
        public DbSet<Outage> Outages { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Server> Servers { get; set; }
        public DbSet<Service> Services { get; set; }

        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options) : base(options)
        {

        }

    }
}
