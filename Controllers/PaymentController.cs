﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;


namespace nOutageAPI.Controllers
{
    [ApiController]
    [Authorize]
    [Route("api/[controller]")]
    public class PaymentController : ControllerBase
    {

        private readonly ApplicationDbContext _context;

        public PaymentController(ApplicationDbContext context)
        {
            _context = context;
        }

        [HttpGet]
        public async Task<IActionResult> GetPayments()
        {
            var companyId = this.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == "CompanyId")?.Value;
            if (companyId == null)
            {
                return Unauthorized();
            }

            var payments = await _context.Payments.Where(payment => payment.Service.Customer.InsurerId == Guid.Parse(companyId)).Select(payment => new {Id = payment.Id, Customer = payment.Service.Customer.Title, Service = payment.Service.Title, Amount = payment.Amount, Outage = payment.Outage.Id, IsPaid = payment.IsPaid }).ToListAsync();
            
            return Ok(payments);
        }

        [HttpPatch("{id:guid}/pay")]
        public async Task<IActionResult> PayInsurance(Guid id)
        {
            var companyId = this.HttpContext?.User?.Claims?.FirstOrDefault(c => c.Type == "CompanyId")?.Value;
            if (companyId == null)
            {
                return Unauthorized();
            }

            var payment = await _context.Payments.Where(payment => payment.Id == id && payment.Service.Customer.Insurer.Id == Guid.Parse(companyId)).FirstOrDefaultAsync();

            if(payment == null)
            {
                return BadRequest();
            }

            payment.IsPaid = true;

            await _context.SaveChangesAsync();

            return Ok();
        }
    }
}
