﻿using Microsoft.AspNetCore.Identity;

namespace nOutageAPI.Authentication
{
    public class ApplicationUser : IdentityUser
    {
        public Guid CompanyId { get; set; }
        public InsuranceCompany Company { get; set; }
    }
}
