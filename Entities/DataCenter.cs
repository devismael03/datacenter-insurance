﻿namespace nOutageAPI.Entities
{
    public class DataCenter
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public List<Server> Servers { get; set; }
    }
}
