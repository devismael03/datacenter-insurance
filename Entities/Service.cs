﻿namespace nOutageAPI.Entities
{
    public class Service
    {
        public Guid Id { get; set; }
        public string Title { get; set; }
        public Priority Priority { get; set; }
        public Customer Customer { get; set; }
        public long SLAMinutes { get; set; }
        public long CurrentDowntimeMinutes { get; set; }
        public List<Server> Servers { get; set; }
        public List<Payment> Payments { get; set; }
    }


    public enum Priority
    {
        Critical,
        High,
        Medium,
        Low
    }
}

